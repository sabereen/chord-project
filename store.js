const sha256 = require('sha256')
const fs = require('fs').promises
const path = require('path')
const config = require('./config')

exports.set = async function set(key, value) {
    const data = JSON.stringify({ key, value })
    await fs.writeFile(path.join(config.storePath, sha256(key)), data)
    return true
}

exports.get = async function get(key) {
    try {
        const data = await fs.readFile(path.join(config.storePath, sha256(key)))
        return JSON.parse(data.toString()).value
    } catch (err) {
        if (err.code === 'ENOENT') {
            return null
        }
        console.error(err)
        throw err
    }
}

exports.delete = async function (key) {
    try {
        await fs.unlink(path.join(config.storePath, sha256(key)))
        return true
    } catch (err) {
        if (err.code === 'ENOENT') return false
        throw err
    }
}
