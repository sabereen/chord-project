const server = require('../rpc-server')
const { Client } = require('jayson/promise')
/** @type {Client} */ let client

beforeAll(() => {
    client = Client.tcp({port: 3000})
})

test('should add 5 + 3 = 8', () => {
    return expect(client.request('add', [5, 3]))
        .resolves.toHaveProperty('result', 8)
})

afterAll(() => {
    server.close()
})

