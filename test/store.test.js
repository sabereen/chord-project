const store = require('../store')

test('Get `null` when key not found', () => {
    return expect(store.get('unsetted')).resolves.toBeNull()
})

test('Get a saved key', async () => {
    await store.set('myKey', 'myValue')
    const value = await store.get('myKey')
    return expect(value).toBe('myValue')
})

test('Delete a saved key', () => {
    return expect(store.delete('myKey')).resolves.toBe(true)
})

test('Error when delete a not exist key', () => {
    return expect(store.delete('hassan')).resolves.toBe(false)
})
