const { Server } = require('jayson/promise')

/** @type {Server} */
const server = Server({
    async add([n1, n2]) {
        return +n1 + +n2
    }
})

const tcpServer = server.tcp()
tcpServer.listen(3000)

module.exports = tcpServer